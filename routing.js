import Node from '../modules/node'
import Relation from '../modules/relation'
import Graph from '../modules/graph'
import Router from '../modules/router'

let nodes = [
	new Node({ id: 1, x: 1, y: 1}),
	new Node({ id: 2, x: 1, y: 2}),
	new Node({ id: 3, x: 2, y: 1}),
	new Node({ id: 4, x: 3, y: 3}),
	new Node({ id: 5, x: 0, y: 2}),
	new Node({ id: 6, x: 2, y: 0}),
]

let relations = [
	new Relation({ id: 1, start: nodes[1], end: nodes[3] })
]

let graph = new Graph({ nodes, relations })
let router = new Router({ graph })

router.getRoute({ start: nodes[1], end: nodes[3]}).then(data => {
	console.dir(data)
})
