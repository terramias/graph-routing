import { getDistance } from '../modules/helper'
import Node from '../modules/node'

test('getDistance should calulate correct length', () => {
	let node1 = new Node({x: 4, y: 7})
	let node2 = new Node({x: 1, y: 1})

	expect(getDistance(node1, node2)).toBe(7)
})