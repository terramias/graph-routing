import Node from '../modules/node'
import Route from '../modules/route'

test('Route should be instantiated correctly', () => {
	let node1 = new Node({id: 1, x: 4, y: 7})
	let instance = new Route({id: 1, start: node1 })

	expect(instance.getLength()).toBe(0)
	expect(instance.getId()).toBe(1)
})

test('Route should return the correct start node', () => {
	let node1 = new Node({id: 1, x: 4, y: 7})
	let instance = new Route({id: 1, start: node1 })

	expect(instance.getStart()).toBe(node1)
})

test('Route should return the correct end node', () => {
	let node1 = new Node({id: 1, x: 4, y: 7})
	let node2 = new Node({id: 2, x: 4, y: 78})
	let node3 = new Node({id: 3, x: 94, y: 7})
	let instance = new Route({id: 1, start: node1 })
	
	instance.addNode(node2)
	expect(instance.getEnd()).toBe(node2)

	instance.addNode(node3)
	expect(instance.getEnd()).toBe(node3)
})

test('Route should has correct length', () => {
	let node1 = new Node({id: 1, x: 4, y: 7})
	let node2 = new Node({id: 2, x: 1, y: 1})
	let instance = new Route({id: 1, start: node1 })

	instance.addNode(node2)

	expect(instance.getLength()).toBe(7)

	expect(instance.getLength()).toBe(7)
})

test('Route should be cloned correctly with same id', () => {
	let node1 = new Node({id: 1, x: 4, y: 7})
	let instance = new Route({id: 1, start: node1 })

	let copy = instance.clone(1)

	expect(instance).not.toBe(copy)
	expect(instance).toEqual(copy)
})

test('Route should be cloned correctly with different id', () => {
	let node1 = new Node({id: 1, x: 4, y: 7})
	let instance = new Route({id: 1, start: node1 })

	let copy = instance.clone(1)

	expect(instance).not.toBe(copy)
	expect(instance).toEqual(copy)
})
