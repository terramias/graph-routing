import Node from '../modules/node'
import Relation from '../modules/relation'
import Graph from '../modules/graph'

test('Graph should be instantiated', () => {
	let nodes = [
		new Node({ id: 1, x: 1, y: 1}),
		new Node({ id: 2, x: 1, y: 2}),
		new Node({ id: 3, x: 2, y: 1}),
		new Node({ id: 4, x: 3, y: 3}),
		new Node({ id: 5, x: 0, y: 2}),
		new Node({ id: 6, x: 2, y: 0}),
	]
	expect(nodes.length).toBe(6)

	let relations = [
		new Relation({ id: 1, start: nodes[1], end: nodes[3] })
	]
	expect(relations.length).toBe(1)

	let graph = new Graph({ nodes, relations })

	expect(graph.getNode({ id: 2 }).getRelations()).toEqual([relations[0]])

	expect(graph.getRelation({ id: 1 })).toEqual(relations[0])

	expect(graph.getNode({ id: 1 })).toEqual(nodes[0])
})