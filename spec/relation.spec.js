import Node from '../modules/node'
import Relation from '../modules/relation'

test('Relation gets instantiated correctly', () => {
	let node1 = new Node({x: 4, y: 7})
	let node2 = new Node({x: 1, y: 1})
	let instance = new Relation({id: 1, start: node1, end: node2})

	expect(instance.getStart()).toBe(node1)
	expect(instance.getEnd()).toBe(node2)
	expect(instance.getId()).toBe(1)
})

test('Relation has correct length', () => {
	let node1 = new Node({x: 4, y: 7})
	let node2 = new Node({x: 1, y: 1})
	let instance = new Relation({id: 1, start: node1, end: node2})

	expect(instance.getLength()).toBe(7)

	expect(instance.getLength()).toBe(7)
})