import Node from '../modules/node'
import Route from '../modules/route'

test('Test that node is created crrectly', () => {
	let instance = new Node({id: 1, x: 4, y: 7})

	expect(instance.getX()).toBe(4)
	expect(instance.getY()).toBe(7)
	expect(instance.getId()).toBe(1)
})

test('Node accepts a route', () => {
	let node1 = new Node({id: 1, x: 4, y: 7})
	let route = new Route( { id: 1, start: node1 })
	node1.setRoute(route)	
	expect(node1.getRoute()).toBe(route)
})

test('Node resets the route', () => {
	let node1 = new Node({id: 1, x: 4, y: 7})
	let route = new Route( { id: 1, start: node1 })
	node1.setRoute(route)	
	expect(node1.getRoute()).toBe(route)
	node1.resetRoute()
	expect(node1.getRoute()).toBeNull()

})

test('Node sets the route if there was another route', () => {
	let node1 = new Node({id: 1, x: 4, y: 7})
	let route1 = new Route( { id: 1, start: node1 })
	let route2 = new Route( { id: 2, start: node1 })
	node1.setRoute(route1)	
	expect(node1.getRoute()).toBe(route1)
	node1.setRoute(route2)
	expect(node1.getRoute()).toBe(route2)

})