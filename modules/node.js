/**
 * Class model for the node
 */
export default class Node {
	/**
	 * Creates an instance of Node
	 *
	 * @param  {{ id: Number, x: Number, y: Number }} config   Instance parameters
	 * @return {Node}	instance of Node
	 */
	constructor( { id, x, y } ) {
		/**
		 * x-value of the nodes coordinates
		 * @type {Number}
		 * @private
		 */
		this.x = x

		/**
		 * y-value of the nodes coordinates
		 * @type {Number}
		 * @private
		 */
		this.y = y

		/**
		 * id of the node
		 * @type {Number}
		 * @private
		 */
		this.id = id

		/**
		 * relations of the node
		 * @type {Relation[]}
		 * @private
		 */
		this.relations = []
	}

	/**
	 * Get the id of the node
	 *
	 * @return {Number} the id of the node
	 */
	getId() {
		return this.id
	}

	/**
	 * Get the x-value of the nodes coordinate
	 *
	 * @return {Number} the x-value of the nodes coordinate
	 */
	getX() {
		return this.x
	}

	/**
	 * Get the y-value of the nodes coordinate
	 *
	 * @return {Number} the y-value of the nodes coordinate
	 */
	getY() {
		return this.y
	}

	/**
	 * Add a relation to the node
	 *
	 * @param {Relation} relation	instance of a relation
	 * @return {Void}
	 */
	addRelation(relation) {
		this.relations.push(relation)
	}

	/**
	 * Get the relations of the node
	 *
	 * @return {Relation[]} Array with the relations of the node
	 */
	getRelations() {
		return this.relations
	}

	/**
	 * Set the route to this node
	 *
	 * @param  {Route} route Route to this node
	 */
	setRoute(route) {
		this.route = route;
	}

	/**
	 * Set the route to this node
	 *
	 * @return  {Route} Route to this node
	 */
	getRoute() {
		return this.route;
	}

	/**
	 * Reset the route to this node
	 *
	 * @return {Null}
	 */
	resetRoute() {
		this.route = null;
	}
}