import Node from './node'
import Relation from './relation'

/**
 * Class model for the node
 */
export default class Graph {
	/**
	 * Creates an instance of Node
	 *
	 * @param  {{ nodes: Node[], relations: Relation[] }} config    Instance parameters
	 * @return {Graph}												Instance of Node
	 */
	constructor( { nodes, relations } ) {
		/**
		 * the nodes of the graph
		 * @type {Node[]}
		 * @private
		 */
		this.nodes = {}

		/**
		 * the relations of the graph
		 * @type {Relation[]}
		 * @private
		 */
		this.relations = {}

		nodes.forEach(this.addNode.bind(this))
		relations.forEach(this.addRelation.bind(this))
	}


	/**
	 * Get the node by id
	 *
	 * @param {{id: Number }} 	node 	the id of the node
	 * @return {Node} 					instance of the node
	 */
	getNode({ id }) {
		return this.nodes[id]
	}


	/**
	 * Get the relation by id
	 *
	 * @param {{id: Number }} 	relation 	the id of the relation
	 * @return {Relation} 					instance of the relation
	 */
	getRelation({ id }) {
		return this.relations[id]
	}

	/**
	 * Add a node to the graph
	 *
	 * @param {Node} node the node to add to the graph
	 */
	addNode(node) {
		this.nodes[node.getId()] = node
	}

	/**
	 * Add a relation to the graph
	 *
	 * @param {Relation} relation the relation to add to the graph
	 */
	addRelation(relation) {
		this.relations[relation.getId()] = relation
		this.nodes[relation.getStart().getId()].addRelation(relation)
		this.nodes[relation.getEnd().getId()].addRelation(relation)
	}
}