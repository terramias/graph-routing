import Node from './node'
import { getDistance } from './helper'

/**
 * Class model for the relation
 */
export default class Relation {
	/**
	 * Creates an instance of Route
	 *
	 * @param  {{ id: number, start: Node, end: Node }} config  Instance parameters
	 * @return {Route}											Instance of Relation
	 */
	constructor( { id, start, end } ) {
		/**
		 * id of the relation
		 * @type {Number}
		 * @private
		 */
		this.id = id

		/**
		 * start node of the relation
		 * @type {Node}
		 * @private
		 */
		this.start = start

		/**
		 * end node of the relation
		 * @type {Node}
		 * @private
		 */
		this.end = end

		/**
		 * lengthof the relation
		 * @type {Number}
		 * @private
		 */
		this.length = 0
	}

	/**
	 * Get the id of the relation
	 *
	 * @return {Number} the id of the relation
	 */
	getId() {
		return this.id

	}

	/**
	 * Get the start node of the relation
	 *
	 * @return {Node} the start node of the relation
	 */
	getStart() {
		return this.start
	}

	/**
	 * Get the end node of the relation
	 *
	 * @return {Node} the end node of the relation
	 */
	getEnd() {
		return this.end
	}

	/**
	 * Get the length of the relation
	 *
	 * @return {Number} the length of the relation
	 */
	getLength() {
		if(this.length === 0) {
			this.length = getDistance(this.start, this.end)
		}

		return this.length
	}

}