"use strict";

import Node from './node'
import { getDistance } from './helper'

/**
 * Class model for the route
 */
export default class Route {
	/**
	 * Creates an instance of Route
	 *
	 * @param  {{ id: number, start: Node, nodes: Array }} config  Instance parameters
	 * @return {Route}	instance of Route
	 */
	constructor({ id, start, nodes = [] }) {
		/**
		 * id of the route
		 * @type {number}
		 * @private
		 */
		this.id = id
		/**
		 * start node of the route
		 * @type {Node}
		 * @private
		 */
		this.start = start

		/**
		 * nodes of the route
		 * @type {Node[]}
		 * @private
		 */
		this.nodes = [ start ]

		/**
		 * length of the route
		 * @type {number}
		 * @private
		 */
		this.length = 0
	}

	/**
	 * Get the route id
	 *
	 * @return {Number} the id of the route
	 */
	getId() {
		return this.id
	}

	/**
	 * Get the start node of the route
	 *
	 * @return {Node} the start node of the route
	 */
	getStart() {
		return this.start
	}

	/**
	 * Get the end node of the route
	 *
	 * @return {Node} the end node of the route
	 */
	getEnd() {
		return this.nodes[this.nodes.length - 1]
	}

	/**
	 * Get the length of the route
	 *
	 * @return {Number}	the length of the route
	 */
	getLength() {
		return this.length
	}

	/**
	 * Add a node to the route
	 *
	 * @param {Node} node	the node instance to add
	 * @return {Void}
	 */
	addNode(node) {
		this.length += getDistance(this.nodes[this.nodes.length - 1], node)
		this.nodes.push(node)
	}


	/**
	 * Clone the route-instance
	 *
	 * @return {Route} a cloned onstance of this route instance
	 */
	clone(newId) {
		let cloned = new Route({
			id: newId,
			start: this.start
		})
		cloned.nodes = this.nodes
		cloned.length = this.length
		return cloned
	}
}