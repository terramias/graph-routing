import Node from './node'

/**
 * Returns the distance between two given nodes
 *
 * @param {Node} node1	the node instance of the start
 * @param {Node} node2	the node instance of the end
 * @return {Number}		the distance between the two nodes
 * @private
 */
export const getDistance = (node1, node2) => {
	return Math.round(
			Math.sqrt(
				Math.pow(node1.getX() - node2.getX(), 2) + Math.pow(node1.getY() - node2.getY(), 2)
			)
		)
}
