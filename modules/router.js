"use strict";

import Node from './node'
import Graph from './graph'
import Route from './route'

/**
 * Router class for getting routes through the graph
 */
export default class Router {

	/**
	 * @param  {{ graph: Graph }} config	Instance of the graph
	 * @return {Router}						Instance of the router
	 */
	constructor({ graph }) {
		/**
		 * instance of the graph
		 * @type {Graph}
		 * @private
		 */
		this.graph = graph
	}

	/**
	 * @param  {{ start: Node, end: Node }}   Start and end node
	 * @return {Array}				list of nodes
	 */
	getRoute( { start, end } ) {
		const visitedNodes = {}
		const visitedRelations = {}
		const paths = {}

		// array mit nodes anlegen
		let nextNodes = [start]

		// walk through array of nodes to go and calculate the possible paths
		let currentNode
		let currentRelations
		let currentNextNodes
		let currentPath

		/**
		 * Map the relation to the next node and length of the relation
		 *
		 * @param  {relation}		Relation
		 * @return {Array}			Array with node and length
		 */
		const mapRelationToNode = relation => {
			let length
			let nodes

			visitedRelations[relation.getId()] = true

			if(currentNode === relation.getStart()) {
				return [ relation.getEnd(), relation.getLength() ]
			}

			if(currentNode === relation.getEnd()) {
				return [ relation.getStart(), relation.getLength() ]
			}
		}

		/**
		 * Filter visited relations
		 *
		 * @param  {Relation}		Relation
		 * @return {Boolean}		True if the relation is not visited, false otherwise
		 */
		const filterVisitedRelations = relation => {
			return this.visitedRelations[relation.getId()] !== undefined && this.visitedRelations[relation.getId()] !== null
		}

		/**
		 * Filter visited nodes
		 *
		 * @param  {Array}			Data of the node
		 * @return {Boolean}		True if the node is not visited, false otherwise
		 */
		const filterVisitedNodes = nodeData => {
			return this.visitedNodes[nodeData[0].getId()] !== undefined && this.visitedNodes[nodeData[0].getId()] !== null
		}


		while(currentNode = nextNodes.shift()) {
			currentRelations = currentNode.getRelations()
			currentNextNodes = currentRelations.
				filter(filterVisitedRelations).
				map(mapRelationToNode).
				filter(filterVisitedNodes)


		}

		// check for the existance of a path solution
		if(true) {
			return new Promise ((resolve, reject) => {
				resolve([])
			})
		}

		// concatenate the two partial paths
	}

	/**
	 * Get the graph instance
	 *
	 * @return {Graph}	instance of the Graph
	 */
	getGraph() {
		return this.graph
	}
}