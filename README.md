# Graph Routing Library

[![build status](https://gitlab.com/terramias/graph-routing/badges/develop/build.svg)](https://gitlab.com/terramias/graph-routing/commits/develop) [![coverage report](https://gitlab.com/terramias/graph-routing/badges/develop/coverage.svg)](http://terramias.gitlab.io/graph-routing/lcov-report/index.html)  [![coverage documentation](https://terramias.gitlab.io/graph-routing/docs/badge.svg)](http://terramias.gitlab.io/graph-routing/docs/index.html) [![Dependency Status](https://www.versioneye.com/user/projects/5978e0a2368b08000f0b8ecf/badge.svg?style=flat-square)](https://www.versioneye.com/user/projects/5978e0a2368b08000f0b8ecf)

## Description

This package provides an easy to handle routing system, that uses graphs (ways and waypoints) and supports wayfinding between several waypoints.

## Node

The basic elements are the nodes. 

## Relation

The connection between two Nodes is a Relation

## Graph

The Graph is the collection of Nodes and Relations

## Route

The Route is a set of Relations and Nodes to reach a Node from another Node

## Router

### Algorithm to find the shortest route

1. create a route from start to nowhere
2. put this route in a routes-object
3. iterate over the routes-object and handle each route
   1. get the end-node
   2. get the next possible nodes
   3. clone the route for each possible node
   4. add the possible nodes to the cloned routes 
   6. check the routes to be the shortest route to the new end-node
     1. if so, set the new route as the to the end-node
     2. if so, remove the old route from the routes-object
     3. if so, add the new route to the routes object

