'use strict';

var _node = require('../modules/node');

var _node2 = _interopRequireDefault(_node);

var _relation = require('../modules/relation');

var _relation2 = _interopRequireDefault(_relation);

var _graph = require('../modules/graph');

var _graph2 = _interopRequireDefault(_graph);

var _router = require('../modules/router');

var _router2 = _interopRequireDefault(_router);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var nodes = [new _node2.default({ id: 1, x: 1, y: 1 }), new _node2.default({ id: 2, x: 1, y: 2 }), new _node2.default({ id: 3, x: 2, y: 1 }), new _node2.default({ id: 4, x: 3, y: 3 }), new _node2.default({ id: 5, x: 0, y: 2 }), new _node2.default({ id: 6, x: 2, y: 0 })];

var relations = [new _relation2.default({ id: 1, start: nodes[1], end: nodes[3] })];

var graph = new _graph2.default({ nodes: nodes, relations: relations });
var router = new _router2.default({ graph: graph });

router.getRoute({ start: nodes[1], end: nodes[3] }).then(function (data) {
	console.dir(data);
});

